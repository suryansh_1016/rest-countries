import ThemeContext from "./Theme"
import { useState } from "react";

const ThemeContextProvider = ({ children }) => {
    const [darkMode, setDarkMode] = useState("false");
    
    return (
        <ThemeContext.Provider value={{darkMode ,setDarkMode }}>
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeContextProvider;