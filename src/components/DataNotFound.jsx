import React , { useContext } from 'react';
import ThemeContext from '../context/Theme';


const DataNotFound = () => {

    const { darkMode } = useContext(ThemeContext);
  return (
    <>
    <div className={darkMode === 'true' ? 'dark notFound' : 'notFound'}>
        Data not found
    </div>
    </>
  )
}

export default DataNotFound