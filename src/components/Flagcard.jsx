import React, { useContext } from 'react';
import ThemeContext from '../context/Theme';
import { useNavigate } from 'react-router-dom';


const Flagcard = ({ imgSrc, name, population, region, capital, id }) => {

  const { darkMode } = useContext(ThemeContext);
  const navigate = useNavigate()

  return (

    <div className={darkMode === 'true' ? 'flagCard dark' : 'flagCard'}
      onClick={() => {
        navigate(`/country/${id}`)
      }}
    >
      <div className='flag' style={{ backgroundImage: `url('${imgSrc}')` }}></div>
      <div className="country-data">
        <h2 className="country-name">{name}</h2>

        <ul>
          <li>Population: <span>{population}</span></li>
          <li>Region: <span>{region}</span></li>
          <li>Capital: <span>{capital}</span></li>
        </ul>
      </div>
    </div>

  )
}

export default Flagcard