import React , {useContext , useEffect} from 'react';
import ThemeContext from '../context/Theme';
import darkLogo from '../assets/dark.svg';
import lightLogo from '../assets/light.svg';

const Navbar = () => {
  const { darkMode, setDarkMode } = useContext(ThemeContext);


  useEffect(() => {
    if (darkMode === 'true') {
      document.body.style.backgroundColor = 'var(--dark-primary)';
    } else {
      document.body.style.backgroundColor = 'var(--light-primary)';
    }
  }, [darkMode]);


  const toggleMode = () => {
   setDarkMode(darkMode === 'false' ? 'true' : 'false')
  }

  return (
    <>
      <nav className={darkMode === 'true' ? 'navbar dark' : 'navbar'} >
        <h1>Where in the World?</h1>
        <div className='mode' onClick={toggleMode}>
          <span className='mode-logo'>
            <img src={darkMode === 'true' ? lightLogo : darkLogo} alt=""  style={{height:'25px' , width:'25px'}}/>
          </span>
          <span>Dark Mode</span>
        </div>
      </nav>
    </>
  )
}

export default Navbar