import React, { useState, useContext } from 'react';
import '../App.css';
import ThemeContext from '../context/Theme';


const Filter = ({ onSearch, setSubRegion, subRegion, subRegionsAvailable, sortBy, setSortBy, curr }) => {

  const [searchQuery, setSearchQuery] = useState('');
  const [selectedRegion, setSelectedRegion] = useState('');
  const [selectedCurrency, setSelectedCurrency] = useState("");


  const handleSearchChange = (event) => {
    const query = event.target.value;
    setSearchQuery(query);
    onSearch(query, selectedRegion, subRegion, sortBy, selectedCurrency);
  }

  function handleRegionChange(event) {
    const region = event.target.value;
    setSelectedRegion(region);
    onSearch(searchQuery, region, subRegion, sortBy, selectedCurrency);
  }

  const { darkMode } = useContext(ThemeContext);

  return (
    <div className='filter'>
      <div className="filter-container">

        <div className={darkMode === 'true' ? 'dark search' : 'search'}>
          <svg className={darkMode === 'true' ? 'dark searchLogo' : 'searchLogo'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" /></svg>
          <input
            className={darkMode === 'true' ? 'dark search-input' : 'search-input'}
            type="text"
            value={searchQuery}
            placeholder='Search for a country'
            onChange={handleSearchChange}
          />
        </div>

        <div className={darkMode === 'true' ? 'dark filter_button' : 'filter_button'}>
          <select className={darkMode === 'true' ? 'dark select' : 'select'} value={selectedRegion} onChange={handleRegionChange}>
            <option value="">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
        <div className={darkMode === 'true' ? 'dark filter_button' : 'filter_button'}>
          <select
            className={darkMode === 'true' ? 'dark select' : 'select'}
            value={subRegion}
            onChange={(e) => {
              const sregion = e.target.value.trim()
              setSubRegion(sregion)
              onSearch(searchQuery, selectedRegion, sregion, sortBy, selectedCurrency)
            }}
          >

            <option value="">Filter By Subregion</option>
            {subRegionsAvailable[selectedRegion] && subRegionsAvailable[selectedRegion].map((subregionCol, index) => {
              return (
                <option key={index} value={`${subregionCol}`}>
                  {subregionCol}
                </option>
              )
            })}

          </select>

        </div>
        <div className={darkMode === 'true' ? 'dark filter_button' : 'filter_button'}>
          <select id="sortby"
            value={sortBy}
            className={darkMode === 'true' ? 'dark select' : 'select'}
            onChange={(e) => {
              setSortBy(e.target.value.trim())
              onSearch(searchQuery, selectedRegion, subRegion, e.target.value.trim(), selectedCurrency);
            }}

          >
            <option value="">sort by</option>
            <option value="population-descending">population (high to low)</option>
            <option value="population-ascending">population (low to high)</option>
            <option value="area-descending">Area (high to low)</option>
            <option value="area-ascending">Area (low to high)</option>
          </select>
        </div>

        <div className={darkMode === 'true' ? 'dark filter_button' : 'filter_button'}>
          <select
            value={selectedCurrency}
            className={darkMode === 'true' ? 'dark select' : 'select'}
            onChange={(e) => {
              setSelectedCurrency(e.target.value.trim());
              onSearch(searchQuery, selectedRegion, subRegion, sortBy, e.target.value.trim());
            }}
          >
            <option value="">sort by currencies</option>
            {
              curr.map((ele , index) => {
                return <option key={index} value={`${ele}`}>{ele}</option>
              })
            }
          </select>
        </div>

      </div>
    </div>
  )
}



export default Filter