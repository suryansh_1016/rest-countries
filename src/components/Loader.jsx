import '../App.css'
import React, { useContext } from 'react';
import ThemeContext from '../context/Theme';

const Loader = () => {

  const { darkMode } = useContext(ThemeContext);

  return (
    <div className="loaderContainer">
      <div className={darkMode === 'true' ? 'dark loader' : 'loader'}></div>
    </div>

  )
}

export default Loader