import React from 'react'
import ThemeContext from '../context/Theme';
import { useContext } from 'react';


const BorderCountries = ({ borderCodes, countryObj }) => {

  const { darkMode } = useContext(ThemeContext);

  return (
    <>
      {
        borderCodes.map((code, index) => {
          const countryName = countryObj[code];
          return (
            <button key={index} className={darkMode === 'true' ? 'dark btn' : 'btn'}>
              {countryName}
            </button>
          );
        })}
    </>

  )
}

export default BorderCountries