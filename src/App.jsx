
import './App.css'
import Home from './pages/Home'
import ThemeContextProvider from './context/ThemeContextProvider'
import Navbar from './components/Navbar'
import Detail from './pages/Detail'
import { Routes, Route } from 'react-router-dom'


function App() {

  return (
    <ThemeContextProvider>
      <Navbar />
      
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/country/:id" element={<Detail />}></Route>
      </Routes>

    </ThemeContextProvider>

  )
}

export default App
