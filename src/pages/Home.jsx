import React, { useEffect, useState , useContext } from 'react'
import Loader from '../components/Loader';
import Flagcard from '../components/Flagcard';
import '../App.css'
import Filter from '../components/Filter';
import ThemeContext from '../context/Theme';


const Home = () => {


    const [countryData, setCountryData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [subRegion, setSubRegion] = useState("");
    const [subRegionsAvailable, setSubRegionsAvailable] = useState({});
    const [sortBy, setSortBy] = useState("");
    const [loadingState, setLoadingState] = useState('true');
    const [currency, setCurrency] = useState([]);

    const { darkMode } = useContext(ThemeContext);

    useEffect(() => {
        fetchData();
    }, [])

    async function fetchData() {
        try {
            let res = await fetch("https://restcountries.com/v3.1/all");
            let data = await res.json();
            setCountryData(data);
            setFilteredData(data);
            setLoadingState('false');

            const currenciesss = [];
            data.forEach((country) => {
                if (country?.currencies != null) {
                    Object.values(country.currencies).forEach((currency) => {
                        if (!currenciesss.includes(currency.name)) {
                            currenciesss.push(currency.name);
                        }
                    });
                }
            })
            setCurrency(currenciesss);


            const subRegion = {};
            data.forEach((country) => {
                if (subRegion[country?.region]) {
                    if (!subRegion?.[country.region].includes(country?.subregion))
                        subRegion?.[country.region].push(country?.subregion);
                } else {
                    subRegion[country.region] = [country.subregion];
                }
            });
            setSubRegionsAvailable(subRegion);


        } catch (error) {
            console.log("Error fetching data", error);
        }
    }

    const handleSearch = (query, region, sregion, sortingBy, selectedCurrency) => {
        const filtered = countryData.filter((country) => {

            // console.log(Object.values(country.currencies)[0].name.toLowerCase())

            return country.name.official.toLowerCase().includes(query.toLowerCase()) &&
                country.region.toLowerCase().includes(region.toLowerCase()) &&
                (sregion === "" || country.subregion == sregion) &&
                (country.currencies && Object.values(country.currencies)[0].name.toLowerCase().includes(selectedCurrency.toLowerCase()))
        })
            .sort((a, b) => {
                if (sortingBy === "population-descending") {
                    return b?.population - a?.population;
                } else if (sortingBy === "population-ascending") {
                    return a?.population - b?.population;
                } else if (sortingBy === "area-descending") {
                    return b?.area - a?.area;
                } else if (sortingBy === "area-ascending") {
                    return a?.area - b?.area;
                }
            })
        setFilteredData(filtered);
    }

    return (
        <>
            <section>

                <Filter
                    onSearch={handleSearch}
                    subRegion={subRegion}
                    setSubRegion={setSubRegion}
                    subRegionsAvailable={subRegionsAvailable}
                    sortBy={sortBy}
                    setSortBy={setSortBy}
                    curr={currency}
                />
                <div className="cards-Container">
                    {
                        loadingState == 'true' ? (
                            <Loader />
                        ) : (
                            filteredData.length !== 0 ? (
                                filteredData.map((country, index) => {
                                    return <Flagcard key={index}
                                        imgSrc={country.flags.png}
                                        name={country.name.common}
                                        population={country.population}
                                        region={country.region}
                                        capital={country.capital}
                                        id={country.ccn3}
                                    />
                                })) : (
                                <>
                                    {/* <DataNotFound /> */}
                                    <div className={darkMode === 'true' ? 'dark notFound' : 'notFound'}>
                                        <h3>No data found</h3>
                                        <p>Sorry, we couldn't find any data matching your search criteria.</p>
                                    </div>
                                </>
                            )
                        )
                    }
                </div>
            </section>
        </>
    )
}

export default Home