import React, { useEffect, useState, useContext } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import Loader from '../components/Loader';
import BorderCountries from '../components/BorderCountries';
import ThemeContext from '../context/Theme';


const Detail = () => {

    const navigate = useNavigate();
    const { id } = useParams();
    const [countryData, setCountryData] = useState();
    const [loadingState, setLoadingState] = useState(true);
    const [countryObj, setcountryObj] = useState(null);

    const { darkMode } = useContext(ThemeContext);


    useEffect(() => {

        const fetchData = async () => {
            try {
                const res = await fetch(`https://restcountries.com/v3.1/alpha/${id}`);
                const data = await res.json();
                setCountryData(data[0]);

                const res2 = await fetch("https://restcountries.com/v3.1/all");
                const data2 = await res2.json();

                const countryCode = {};
                data2.map((ele) => {
                    if (!countryCode[ele.cca3]) {
                        countryCode[ele.cca3] = ele.name.common;
                    }
                })

                setcountryObj(countryCode);

            }
            catch (error) {
                console.error("Error fetching data:", error);
            }
            finally {
                setLoadingState(false);
            }
        };
        fetchData();
    }, [id]);



    return (
        <>
            {
                loadingState === 'true' ? <Loader /> :
                    (<div className="detail-section">
                        <button className={darkMode === 'true' ? 'dark backbtn btn' : 'btn backbtn'} onClick={() => {
                            navigate(-1)
                        }}>
                            <svg className={darkMode === 'true' ? 'dark backsvg' : 'backsvg'} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" /></svg>
                            Back
                        </button>
                        <div className="detail-container">
                            <div className="flag-detail-img">
                                <img src={countryData?.flags?.svg} alt="" />
                            </div>
                            <div className={darkMode === 'true' ? 'dark detail-content' : 'detail-content'}>
                                <h1>
                                    {countryData?.name?.official}
                                </h1>
                                <div className="country-details">
                                    <ul>
                                        <li><b>Native Name: </b>{countryData ? (Object.values(countryData?.name?.nativeName)[0]?.common) : <></>}</li>
                                        <li><b>Population: </b>{countryData?.population}</li>
                                        <li><b>Region: </b>{countryData?.region}</li>
                                        <li><b>Sub Region: </b>{countryData?.subregion}</li>
                                        <li><b>Capital: </b>{countryData?.capital}</li>
                                    </ul>
                                    <ul>
                                        <li><b>Top Level Domain: </b>{countryData?.tld}</li>
                                        <li><b>Currencies: </b>{countryData ? Object.values(countryData?.currencies)[0].name : <></>}</li>
                                        <li><b>Languages: </b>{
                                            countryData ? Object.values(countryData?.languages).join(", ") : <></>
                                        }</li>
                                    </ul>
                                </div>
                                <div className='neighbours'>
                                    <h2>Border Countries: </h2>
                                    {
                                        countryObj && countryData?.borders ? (
                                            <BorderCountries borderCodes={countryData.borders} countryObj={countryObj} />)
                                            : (<p> No border countries</p>)

                                    }
                                </div>
                            </div>
                        </div>
                    </div>)
            }
        </>
    )
}

export default Detail